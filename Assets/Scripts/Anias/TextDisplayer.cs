﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextDisplayer : MonoBehaviour
{
    string message;
    Text textComp;

    // Use this for initialization
    void Start()
    {
        textComp = GetComponent<Text>();
        message = textComp.text;
        textComp.text = "";
    }

    void Update ()
    {
        if(Input.anyKey)
        {
            textComp.text = message;
        }
    }
}