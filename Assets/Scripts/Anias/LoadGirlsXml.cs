﻿using UnityEngine;
using System.Collections;
using System.Xml;

public struct Girl
{
    public string prefab;
    public string answ_eyes;
    public string food;
    public string answ_drink;
    /*
    public string fav_genre
    public string fav_movie
    public string fav_weed
    public string answ_fav_band
    public string fav_song
    public string answ_fav_song
    */
}

public class LoadGirlsXml : MonoBehaviour {
    public GameObject[] girls;
    public static LoadGirlsXml instance = null;
    public TextAsset GameAsset;
    //public string currentBottomText;
    public Girl aGirl;
    XmlDocument xmlDoc;

    void Awake()
    {
        aGirl = new Girl();
        // Debug.Log("LoadGirlsXml Awake");
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
    // Use this for initialization
    void Start () {
        GetData();
    }

    public void GetData()
    {
        xmlDoc = new XmlDocument(); // xmlDoc is the new xml document.
        xmlDoc.LoadXml(GameAsset.text); // load the file.
        //XmlNodeList girlsList = xmlDoc.GetElementsByTagName("girl"); // array of the level nodes.
        XmlNodeList girlsList = xmlDoc.SelectNodes("girls/girl"); // You can also use XPath here
        //Debug.Log(girlsList.Count);
        //Debug.Log(xPath);
        int gNo = Random.Range(1, 4);
        string xPath = "girls/girl["+ gNo.ToString()+ "]";

        XmlNode girlNode = xmlDoc.SelectSingleNode(xPath);
        aGirl.prefab = girlNode.SelectSingleNode("prefab").InnerText;
        aGirl.food = girlNode.SelectSingleNode("food").InnerText;
        aGirl.answ_eyes = girlNode.SelectSingleNode("answ_eyes").InnerText;
        aGirl.answ_drink = girlNode.SelectSingleNode("answ_drink").InnerText;
        Debug.Log("aGirl: "+aGirl.prefab+" "+aGirl.food+" "+aGirl.answ_drink+" "+aGirl.answ_eyes);
        //create actual prefab and scale it
        GameObject theGirl = Instantiate(girls[gNo-1], new Vector3(-4.27f, -2.06f, 90f), Quaternion.identity) as GameObject;
        //theGirl.transform.localScale *= 2;
        GameManager.instance.girlAnimator = theGirl.GetComponent<Animator>();
       // GameManager.instance.ontits = false;
    }//Get Data
}
