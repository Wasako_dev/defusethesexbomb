﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Xml;
using System.IO;

public class LoadXmlData : MonoBehaviour
{

    public static LoadXmlData instance = null;
    public TextAsset GameAsset;
    public string currentBottomText;
    //public string currentCorrectAnsw;
    XmlDocument xmlDoc;
    public XmlNode level;
    public XmlNode levelItem;
    IEnumerator levelEnum;
    IEnumerator ItemEnum;
    void Awake()
    {
        //Debug.Log("LoadXmlData Awake");
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        //DontDestroyOnLoad(gameObject);
    }
    // Use this for initialization
    void Start()
    { //Timeline of the Level creator
        GetData();
    }
    void Update() {
     //   handleInput();
    }
   
    public void nextItem()
    {
        if (ItemEnum.MoveNext())
        {
            levelItem = (XmlNode)ItemEnum.Current;
            parseItem(levelItem);
        }
        else
        {
            Debug.Log("level end");
            nextLevel();
        }
    }

    public void nextLevel() {
        if (levelEnum.MoveNext())
        {
            level = (XmlNode)levelEnum.Current;
            Debug.Log("xx" + level.OuterXml);
            ItemEnum = level.GetEnumerator();
            nextItem();
        }
        else
            Debug.Log("game end");
    }

    public void GetData()
    {
        xmlDoc = new XmlDocument(); // xmlDoc is the new xml document.
        xmlDoc.LoadXml(GameAsset.text); // load the file.
        //Debug.Log("We just started");
        XmlNodeList levelsList = xmlDoc.GetElementsByTagName("level"); // array of the level nodes.
        //XmlNodeList nodes = root.SelectNodes("levels\level"); // You can also use XPath here

        XmlNode root = xmlDoc.DocumentElement;
        levelEnum = root.GetEnumerator();
        nextLevel();
        /*
        //for all levels
        foreach (XmlNode action in levelsList)
        {
            //for nodes in level
            XmlNodeList levelcontent = action.ChildNodes;
            foreach (XmlNode levelsItems in levelcontent) // levels itens nodes.
            {
                Debug.Log(levelsItems.Name);
                if (levelsItems.Name == "name")
                {
             //       Debug.Log(levelsItens.InnerText); // put this in the dictionary.
                }//if name
            }//foreach levelcontent
        } //foreach level
        */
    }//Get Data
    void parseItem(XmlNode level_item)
    {
        
        switch (level_item.Name)
        {
            case "info":
                currentBottomText = level_item.InnerText.Replace("#food", LoadGirlsXml.instance.aGirl.food);
                break;
            case "quest":
                currentBottomText = level_item.InnerText.Replace("#food", LoadGirlsXml.instance.aGirl.food);
                break;
            case "answers":
                string[] answers= new string[4];
                string[] meaning = new string[4];
                XmlNodeList answNodes = level_item.SelectNodes("answer");
                for (int i = 0; i < answers.Length; i++)
                {
                    answers[i]=answNodes[i].InnerText;
                    //meaning[i] = answNodes[i].Attributes("@id");
                }
                GameManager.instance.showButtons(answers, meaning);
                //Debug.Log("answers!");
                break;
            case "won":
                GameManager.instance.hideButtons();
                currentBottomText = level_item.InnerText;
                break;
            case "lost":
                GameManager.instance.hideButtons();
                currentBottomText = level_item.InnerText;
                break;
            default:
                //Debug.Log("Invalid tag" + level_item.Name);
                break;
        }
    }//parseItem
}