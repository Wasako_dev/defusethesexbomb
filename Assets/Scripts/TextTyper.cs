﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextTyper : MonoBehaviour
{

    public float letterPause ;
    public AudioClip blah1;
    public AudioClip blah2;
    public AudioClip blah3;
    //int textIndex;//if you want to iterate using for loop
    string message;
    Text textComp;
    public bool isTalking;

    // Use this for initialization
    void Start()
    {
        textComp = GetComponent<Text>();
        message = textComp.text;
        //textIndex = 0;
        textComp.text = "";
        StartCoroutine("TypeText");
    }

    void Update ()
    {
        handleInput();
        //if (Input.anyKey)
        
        
 
    }
    public void handleInput()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            StopCoroutine("TypeText");
            isTalking = false;
            GameManager.instance.talkBitch();
            textComp.text = message;
            //SoundManager.instance.RandomizeSfx(blah1, blah2); //, blah3);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) )
        {
            Debug.Log("space works");
            LoadXmlData.instance.nextItem();
            message = LoadXmlData.instance.currentBottomText;
            textComp.text = "";
            StopCoroutine("TypeText");
            StartCoroutine("TypeText");
        }
    }
    IEnumerator TypeText()
    {
        isTalking = true;
        GameManager.instance.talkBitch();
        foreach (char letter in message)  // When you iterate over a string it is automatically changes to char array = message.ToCharArray();
        //for (int i = textIndex; i< message.Length; i++) //if you want to iterate using for loop
        {
            textComp.text += letter; // letter is message[i];
            //if (blah1 && blah2 && textComp.text.Length % 4 == 0)
              //  SoundManager.instance.RandomizeSfx(blah1, blah2); //, blah3);
         // textIndex = i; //if you want to iterate using for loop
            yield return new WaitForSeconds(letterPause);
        }
        isTalking = false;
        GameManager.instance.talkBitch();
    }
}