﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class endingHack : MonoBehaviour {
    public Sprite Win1, Win2, Win3, Lose;
    public Image screen;
    public Animator cameraAnimator;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.M))
        {
            screen.sprite = Win1;
            screen.GetComponent<Image>().enabled = true;
        }
        if (Input.GetKey(KeyCode.B)) 
        {
            screen.GetComponent<Image>().enabled = true;
            screen.sprite = Win2;
        }
        if (Input.GetKey(KeyCode.D))
        {
            screen.GetComponent<Image>().enabled = true;
            screen.sprite = Win3;
        }
        if (Input.GetKey(KeyCode.L)) 
        {
            screen.GetComponent<Image>().enabled = true;
            screen.sprite = Lose;
        }
        if (Input.GetKey(KeyCode.C)) cameraAnimator.SetBool("ontits", false);
        if (Input.GetKey(KeyCode.R)) Application.LoadLevel(0);
    }
}
